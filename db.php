<?php

$db = new PDO('sqlite::memory:');
#$db = new PDO('mysql:dbname=birdsdb;host=localhost', 'root', 'toor');
if(! $db instanceof PDO)
{
    return false;
}

$create_table = $db->prepare("CREATE TABLE birds (id varchar(50), label varchar(50), name varchar(50));");
if(! $create_table instanceof PDOStatement)
{
    return false;
}

if(!$create_table->execute())
{
    return false;
}

// Let's insert some data!
function insert(PDOStatement $bird, $id, $label, $name)
{
    $bird->bindParam(':id', $id);
    $bird->bindParam(':label', $label);
    $bird->bindParam(':name', $name);

    $bird->execute();
}
{
    global $db;
    $bird = $db->prepare("INSERT INTO birds VALUES (:id, :label, :name);");
    if(!$bird instanceof PDOStatement)
    {
        return false;
    }
    insert($bird, 'Mimus polyglottos', 'Northern Mockingbird', 'Northern Mockingbird');
    
    // Since we're lazy we're just going to add the rest like this...
    $data = json_decode('
    [{"id":"Monticola solitarius","label":"Blue Rock Thrush","value":"Blue Rock Thrush"},{"id":"Anthus petrosus","label":"Rock Pipit","value":"Rock Pipit"},{"id":"Alectoris graeca","label":"Rock Partridge","value":"Rock Partridge"},{"id":"Monticola saxatilis","label":"Rufous-tailed Rock Thrush","value":"Rufous-tailed Rock Thrush"},{"id":"Lagopus mutus","label":"Rock Ptarmigan","value":"Rock Ptarmigan"},{"id":"Emberiza cia","label":"Rock Bunting","value":"Rock Bunting"}]');

    foreach($data as $item)
    {
        insert($bird, $item->id, $item->label, $item->value);
    }
}

function getBirds($term)
{
    global $db;

    // Enforce rule of minimum three letters...
    if(strlen($term) < 3)
    {
        return array();
    }
    $select = $db->prepare("SELECT id, label, name as value FROM birds WHERE value LIKE :term"); // OR label LIKE ':term' OR id LIKE ':term' LIMIT 15");
    if(! $select instanceof PDOStatement)
    {
        return array();
    }
    $wild_term = "%$term%";
    $select->execute(array($wild_term));
    $data = $select->fetchAll( PDO::FETCH_OBJ );

    return ($data !== false)? $data : array();
}

return true;
