<?php

$db_init = require_once('db.php');

if($db_init === false)
{
    global $db;
    var_dump($db->errorInfo());
    die('Database error');
}
if(array_key_exists('term', $_GET))
{
    $term = $_GET['term'];
} 
else if(isset($argc) && $argc > 1)
{
    $term = $argv[1];
}
else
{
    die(json_encode(array('error' => 'Usage: "json.php?term=ccc" or "php json.php ccc"')));
}
echo json_encode(getBirds($term));
